const Metalsmith = require('metalsmith')
const markdown = require('metalsmith-markdown')
const layouts = require('metalsmith-layouts')
const permalinks = require('metalsmith-permalinks')
const browserSync = require('metalsmith-browser-sync')
const sass = require('metalsmith-sass')
const assets = require('metalsmith-static')

Metalsmith(__dirname)
  .metadata({
    title: 'My Static Site & Blog',
    description: 'Its about saying »Hello« to the World.',
    generator: 'Metalsmith',
    url: 'http://www.metalsmith.io/'
  })
  .source('./src')
  .destination('./build')
  .clean(false)
  .use(markdown())
  .use(permalinks())
  .use(assets({
    src: 'assets',
    dest: '/assets'
  }))
  .use(layouts({
    engine: 'handlebars',
    partials: 'partials'
  }))
  .use(sass({
    outputStyle: 'compressed',
  }))
  .use(browserSync({
    server: 'build',
    files: ['src/**/*', 'layouts/**/*.html', 'partials/**/*.html', 'assets/**/*']
  }))
  .build((err) => {
    if (err) { throw err }
  })
